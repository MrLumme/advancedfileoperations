package lumCode.advancedFileOperations.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import org.apache.commons.io.FileUtils;

public class Copiers {
	private static final Random r = new Random();

	public static void copyRandomFiles(File source, File destination, int amount, boolean includeSubfolders,
			String[] whitelist) {
		ArrayList<File> avail = new ArrayList<>();

		if (includeSubfolders) {
			Finders.findAllFiles(source, avail);
		} else {
			for (File fil : source.listFiles()) {
				if (fil.isFile()) {
					avail.add(fil);
				}
			}
		}

		for (int i = 0; i < amount; i++) {
			if (avail.size() == 0) {
				break;
			}
			File fil = avail.get(r.nextInt(avail.size()));
			avail.remove(fil);

			boolean inList = false;
			if (whitelist != null && whitelist.length > 0) {
				for (String str : whitelist) {
					if (fil.getName().indexOf(str) != -1) {
						inList = true;
						break;
					}
				}
			} else {
				inList = true;
			}

			if (inList) {
				try {
					FileUtils.copyFileToDirectory(fil, destination);
				} catch (IOException e) {
					i--;
					e.printStackTrace();
				}
			} else {
				i--;
			}
		}
	}
}
