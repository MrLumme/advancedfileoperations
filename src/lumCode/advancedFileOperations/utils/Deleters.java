package lumCode.advancedFileOperations.utils;

import java.io.File;

public class Deleters {
	private static long deleteCount = 0;
	private static long failedCount = 0;

	public static void deleteEmptyFolders(File deploy) {
		recurseEmptyFolder(deploy);
		System.out.println("Deleted " + deleteCount + " folders, failed to delete " + failedCount + " folders.");
	}

	private static boolean recurseEmptyFolder(File dir) {
		File[] files = dir.listFiles();
		boolean hasFiles = false;

		for (int i = 0; i < files.length; i++) {
			if (files[i].isDirectory()) {
				if (!recurseEmptyFolder(files[i])) {
					if (files[i].delete()) {
						System.out.println("Deleted: " + files[i].getAbsolutePath());
						deleteCount++;
					} else {
						System.out.println("Failed: " + files[i].getAbsolutePath());
						failedCount++;
					}
				}
			} else if (files[i].isFile()) {
				hasFiles = true;
			}
		}

		return hasFiles;
	}
}
