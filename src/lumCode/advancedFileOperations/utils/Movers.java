package lumCode.advancedFileOperations.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import org.apache.commons.io.FileUtils;

public class Movers {

	/*
	 * Moves a folder one directory out and deletes the old parent directory, if the
	 * parent only contains one folder
	 */

	public static void moveSingleFoldersOut(File deployDirectory, boolean recurse) {
		ArrayList<File> parents = new ArrayList<>();
		UUID uuid = UUID.randomUUID();
		File tempDeploy = new File(deployDirectory.getAbsolutePath() + "/" + uuid.toString());
		tempDeploy.mkdirs();

		for (File fil : deployDirectory.listFiles()) {
			if (recurse) {
				moveSingleFoldersOut(fil, recurse);
			}
			File[] con = fil.listFiles();
			if (con.length == 1 && con[0].isDirectory()) {
				parents.add(fil);
				System.out.println("Moving: " + fil.getAbsolutePath()
						.substring(deployDirectory.getAbsolutePath().length() - deployDirectory.getName().length()));
				try {
					FileUtils.moveToDirectory(con[0], tempDeploy, true);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		for (File p : parents) {
			p.delete();
		}
		try {
			Thread.sleep(500);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		for (File t : tempDeploy.listFiles()) {
			try {
				FileUtils.moveToDirectory(t, tempDeploy, true);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			Thread.sleep(500);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		tempDeploy.delete();
	}

	public static void moveSingleFoldersOut(File deployDirectory) {
		moveSingleFoldersOut(deployDirectory, true);
	}
}
