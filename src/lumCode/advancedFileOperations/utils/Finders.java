package lumCode.advancedFileOperations.utils;

import java.io.File;
import java.util.ArrayList;

public class Finders {

	/*
	 * Find All Files
	 */

	public static void findAllFiles(File deployDirectory, ArrayList<File> output, String[] extentions) {
		searchFile(deployDirectory, output, extentions);
	}

	public static void findAllFiles(File deployDirectory, ArrayList<File> output) {
		searchFile(deployDirectory, output);
	}

	public static ArrayList<File> findAllFiles(File deployDirectory, String[] extentions) {
		ArrayList<File> output = new ArrayList<>();
		findAllFiles(deployDirectory, output, extentions);
		return output;
	}

	public static ArrayList<File> findAllFiles(File deployDirectory) {
		ArrayList<File> output = new ArrayList<>();
		searchFile(deployDirectory, output);
		return output;
	}

	/*
	 * Search file
	 */

	private static void searchFile(File fil, ArrayList<File> fileList, String[] extensions) {
		if (fil.isDirectory()) {
			File[] subDirFiles = fil.listFiles();

			for (int i = 0; i < subDirFiles.length; i++) {
				searchFile(subDirFiles[i], fileList, extensions);
			}
		} else if (fil.isFile()) {
			for (String str : extensions) {
				if (fil.getAbsolutePath().toLowerCase().endsWith(str)) {
					fileList.add(fil);
					break;
				}
			}
		}
	}

	private static void searchFile(File fil, ArrayList<File> fileList) {
		if (fil.isDirectory()) {
			File[] subDirFiles = fil.listFiles();

			for (int i = 0; i < subDirFiles.length; i++) {
				searchFile(subDirFiles[i], fileList);
			}
		} else if (fil.isFile()) {
			fileList.add(fil);
		}
	}

	/*
	 * Find All Folder
	 */

	public static void findAllFolders(File deployDirectory, ArrayList<File> output) {
		searchFolder(deployDirectory, output);
	}

	public static ArrayList<File> findAllFolders(File deployDirectory) {
		ArrayList<File> output = new ArrayList<>();
		findAllFolders(deployDirectory, output);
		return output;
	}

	/*
	 * Search folder
	 */

	private static void searchFolder(File fil, ArrayList<File> fileList) {
		if (fil.isDirectory()) {
			fileList.add(fil);
			File[] subDirFiles = fil.listFiles();

			for (int i = 0; i < subDirFiles.length; i++) {
				searchFolder(subDirFiles[i], fileList);
			}
		}
	}
}
