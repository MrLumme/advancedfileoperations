package lumCode.advancedFileOperations.core;

import java.io.File;

import lumCode.advancedFileOperations.utils.Copiers;
import lumCode.advancedFileOperations.utils.Deleters;
import lumCode.advancedFileOperations.utils.Movers;

public class Main {
	private static Operation op = null;
	private static File deploy = null;
	private static File source = null;
	private static File target = null;
	private static int amount = -1;
	private static String[] list = null;
	private static boolean subfolders = false;

	public static void main(String[] args) {
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-list")) {
				showOpList();
			}
			if (args[i].equals("-op")) {
				i++;
				if (args[i].equals("deleteEmptyFolders")) {
					op = Operation.DELETE_EMPTY_FOLDERS;
				} else if (args[i].equals("copyRandomFiles")) {
					op = Operation.COPY_RANDOM_FILES;
				} else if (args[i].equals("moveSingleFoldersOut")) {
					op = Operation.MOVE_SINGLE_FOLDERS_OUT;
				}
			}
			if (args[i].equals("-a")) {
				i++;
				amount = Integer.parseInt(args[i]);
			}
			if (args[i].equals("-t")) {
				i++;
				target = new File(args[i]);
			}
			if (args[i].equals("-s")) {
				i++;
				source = new File(args[i]);
			}
			if (args[i].equals("-d")) {
				i++;
				deploy = new File(args[i]);
			}
			if (args[i].equals("-u")) {
				subfolders = true;
			}
			if (args[i].equals("-l")) {
				i++;
				list = args[i].split(",");
				for (int j = 0; j < list.length; j++) {
					list[j] = list[j].trim();
				}
			}
		}

		if (op == Operation.DELETE_EMPTY_FOLDERS && deploy != null) {
			Deleters.deleteEmptyFolders(deploy);
		} else if (op == Operation.COPY_RANDOM_FILES && source != null && target != null && amount > 0) {
			Copiers.copyRandomFiles(source, target, amount, subfolders, list);
		} else if (op == Operation.MOVE_SINGLE_FOLDERS_OUT && deploy != null) {
			Movers.moveSingleFoldersOut(deploy, subfolders);
		} else {
			showHelp();
		}
	}

	public static void showHelp() {
		System.out.println("Advanced File Operations\n" + "v1.0\n\n" + "Operation switches:\n"
				+ "-op\tOperation to run.\n" + "-list\tPrints a list of commands and their requirements." + "\n");
	}

	public static void showOpList() {
		System.out.println("Operations:\n"
				+ "deleteEmptyFolders\n\tDeletes empty folders from a given deploy directory [-d <PATH>].\n\n"
				+ "copyRandomFiles\n\tCopies a given amount [-a <NUMBER>] of randomly selected files from a given source directory "
				+ "[-s <PATH>] to a given target directory [-t <PATH>]. It can include files in subfolders with [-u]. It can also "
				+ "limit what files are chosen by applying a whitelist with [-l \"<LIST>\"]. The list separator is a comma [,].\n\n"
				+ "moveSingleFoldersOut\n\tIdentifies folders in a deploy directory [-d <PATH>] with a folder as its only content. "
				+ "Then moves the child folder into the same directory as the parent. Then it deletes the parent folder.");
	}
}
