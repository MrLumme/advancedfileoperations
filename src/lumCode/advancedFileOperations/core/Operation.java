package lumCode.advancedFileOperations.core;

public enum Operation {
	DELETE_EMPTY_FOLDERS, COPY_RANDOM_FILES, MOVE_SINGLE_FOLDERS_OUT;
}
